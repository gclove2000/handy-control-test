﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Models
{
    public class TemplateDate
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public long Phone { get; set; }

        public enum SexEnum { 男, 女, 保密 }

        public SexEnum Sex { get; set; }
    }
}
