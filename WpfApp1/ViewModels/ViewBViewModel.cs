﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Models;

namespace WpfApp1.ViewModels
{
    public class ViewBViewModel:INotifyCollectionChanged
    {
        [DoNotNotify]
        public List<TemplateDate> TemplateDates { get; set; }
        public ViewBViewModel()
        {
            TemplateDates = new List<TemplateDate>() {
                new TemplateDate(){Name="小明",Age = 16,Phone = 13214324920,Sex = TemplateDate.SexEnum.男},
                new TemplateDate(){Name="小红",Age = 17,Phone = 38188949204,Sex = TemplateDate.SexEnum.女}
            };
        }

        public event NotifyCollectionChangedEventHandler? CollectionChanged;
    }
}
