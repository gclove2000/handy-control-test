﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HandyControl.Controls;
using HandyControl.Data;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.ViewModels
{
    public partial class ViewAViewModel:ObservableObject, INotifyCollectionChanged
    {

        [ObservableProperty]
        private string name = "Name";

        public event NotifyCollectionChangedEventHandler? CollectionChanged;

        public RelayCommand InfoBtn { get; set; }

        public RelayCommand SuccessBtn { get; set; }

        public RelayCommand WarningBtn { get; set; }    

        public RelayCommand ErrorBtn { get; set; }

        public RelayCommand AskBtn { get; set; }

        public RelayCommand SeriousBtn { get; set; }

        public RelayCommand ClearBtn { get; set; }

        public RelayCommand InfoGlobalBtn { get; set; }

        public RelayCommand SuccessGlobalBtn { get; set; }

        public RelayCommand WarningGlobalBtn { get; set; }

        public RelayCommand ErrorGlobalBtn { get; set; }

        public RelayCommand AskGlobalBtn { get; set; }

        public RelayCommand SeriousGlobalBtn { get; set; }

        public RelayCommand ClearGlobalBtn { get; set; }

        public ViewAViewModel() {
            Name = "Name";

            InfoBtn = new RelayCommand(() =>
            {
                Growl.Info("消息");
            });

            SuccessBtn = new RelayCommand(() => {
                Growl.Success("成功！");
            });

            WarningBtn = new RelayCommand(() => {
                Growl.Warning("警告");
            });
            ErrorBtn = new RelayCommand(() => {
                Growl.Error("错误");
            });

            AskBtn = new RelayCommand(() => {
                Growl.Ask("询问", isConfirmed =>
                {
                    Growl.Info($"询问结果[{isConfirmed}]");
                    return true;
                });
            
            });
            SeriousBtn = new RelayCommand(() => {
                Growl.Fatal("严重");
            });
            ClearBtn = new RelayCommand(() => { 
            
                Growl.Clear();
            });


            InfoGlobalBtn = new RelayCommand(() =>
            {
                Growl.InfoGlobal("消息");
            });
            SuccessGlobalBtn = new RelayCommand(() => {
                Growl.SuccessGlobal("成功！");
            });

            WarningGlobalBtn = new RelayCommand(() => {
                Growl.WarningGlobal("警告");
            });
            ErrorGlobalBtn = new RelayCommand(() => {
                Growl.ErrorGlobal("错误");
            });

            AskGlobalBtn = new RelayCommand(() => {
                Growl.AskGlobal("询问", isConfirmed =>
                {
                    Growl.InfoGlobal($"询问结果[{isConfirmed}]");
                    return true;
                });

            });
            SeriousGlobalBtn = new RelayCommand(() => {
                Growl.FatalGlobal("严重");
            });
            ClearGlobalBtn = new RelayCommand(() => {

                Growl.ClearGlobal();
            });
        }
    }
}
